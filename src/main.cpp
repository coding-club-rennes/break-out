/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
** Last update Mon Feb  2 14:07:47 2015 Alexis Bertholom
*/

#include <unistd.h>
#include "Colors.hpp"
#include "Input.hpp"
#include "SDLDisplay.hpp"

// Largeur de la fenetre, en pixels.
const int WINDOW_WIDTH = 800;
// Hauteur de la fenetre, en pixels.
const int WINDOW_HEIGHT = 600;

// Largeur d'une brique, en pixels.
const int BRICK_WIDTH = 80;
// Hauteur d'une brique, en pixels.
const int BRICK_HEIGHT = 40;

// Largeur de la raquette, en pixels.
const int BAT_WIDTH = 240;
// Hauteur de la raquette, en pixels.
const int BAT_HEIGHT = 30;
// Position de la raquette, en pixels sur l'axe Y.
const int BAT_POSITION = WINDOW_HEIGHT - 60;

// Largeur de la balle, en pixels.
const int BALL_WIDTH = 15;
// Hauteur de la balle, en pixels.
const int BALL_HEIGHT = 15;

int main() {
  int bricks[5][10] = {
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
      {0, 1, 0, 1, 0, 0, 0, 0, 0, 0}, {0, 1, 0, 1, 0, 0, 0, 0, 0, 0},
      {0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
  };
  SDLDisplay display("Break Out", WINDOW_WIDTH, WINDOW_HEIGHT);
  Input input;
  // Position de la raquette, en pixels sur l'axe X.
  int bat_x = (WINDOW_WIDTH - BAT_WIDTH) / 2;
  // Stocke l'état de la touche espace
  bool pressed_space = false;
  // Stocke l'existence de la balle
  bool ball_exists = false;
  // Stocke la position de la balle, en pixels sur l'axe X.
  int ball_x = 0;
  // Stocke la position de la balle, en pixels sur l'axe Y.
  int ball_y = 0;

  while (!(input.shouldExit())) {
    display.clearScreen();
    // Déplacer la balle si elle existe
    // ...

    // Gestion des évènements clavier
    if (input.getKeyState(SDL_SCANCODE_LEFT)) {
      if (bat_x > 0) {
        bat_x = bat_x - 1;
      }
    }
    if (input.getKeyState(SDL_SCANCODE_RIGHT)) {
      // begin:sample
      if (bat_x < WINDOW_WIDTH - BAT_WIDTH) {
        bat_x = bat_x + 1;
      }
      // end:sample
    }
    if (input.getKeyState(SDL_SCANCODE_SPACE)) {
      pressed_space = true;
    } else if (pressed_space == true) {
      pressed_space = false;
      // Si la barre espace est relachée, lancer la balle.
      // ...
    }

    // Affichage des différents éléments du jeu
    //
    // Affichage des briques
    for (int y = 0; y < ARRAY_SIZE(bricks); ++y) {
      for (int x = 0; x < ARRAY_SIZE(bricks[0]); ++x) {
        if (bricks[y][x] > 0) {
          display.putRect(BRICK_WIDTH * x, BRICK_HEIGHT * y, BRICK_WIDTH,
                          BRICK_HEIGHT, Colors::Black);
          display.putRect(BRICK_WIDTH * x + 1, BRICK_HEIGHT * y + 1,
                          BRICK_WIDTH - 1, BRICK_HEIGHT - 1, Colors::Red);
        }
      }
    }
    if (ball_exists) {
      display.putRect(ball_x, ball_y, BALL_WIDTH, BALL_HEIGHT, Colors::Blue);
    }
    // Affichage de la raquette
    display.putRect(bat_x, BAT_POSITION, BAT_WIDTH, BAT_HEIGHT, Colors::White);

    // Your code ends here
    display.refreshScreen();
    input.flushEvents();
    usleep(1000);
  }
  return (0);
}
