//
// FileError.hpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 00:01:35 2014 bertho_d
// Last update Tue Aug  5 20:44:23 2014 bertho_d
//

#ifndef FILEERROR_HPP_
# define FILEERROR_HPP_

# include "Error.hpp"

enum			t_fileErrCode
{
  OPEN_ERROR = 0,
  READ_ERROR,
  BAD_SHADER,
  BAD_GL_PROGRAM
};

class			FileError : public Error
{
public:
  FileError(t_fileErrCode errcode, const char *filename = NULL);

protected:
  t_fileErrCode		_errcode;

private:
  static const char	*fileErrorMessages[];
};

#endif
