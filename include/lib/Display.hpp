/*
** Display.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:07:09 2015 Alexis Bertholom
** Last update Tue Jan 27 14:12:16 2015 Alexis Bertholom
*/

#ifndef DISPLAY_HPP_
# define DISPLAY_HPP_

# include "types.hpp"

class		Image;

class		Display
{
public:
  virtual ~Display() {}

public:
  virtual void	putPixel(Uint x, Uint y, Color color) = 0;
  virtual void	putRect(Uint x, Uint y, Uint w, Uint h, Color color) = 0;
  virtual void	putImage(Uint x, Uint y, Image& img, Uint w, Uint h) = 0;
  virtual void	clearScreen() = 0;
  virtual void	fillScreen(Color color) = 0;
  virtual void	refreshScreen() = 0;
};

#endif
